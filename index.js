const expr = require('express');

const db = require('./db.js');


var app = expr();
var port = process.env.PORT;



app.get('/', function(req, res)
{
    db.query('SELECT * FROM app', [], function(err, resp)
    {
        if(err)
        {
            res.send(err)
        }
        else
        {
            res.send(resp.rows);
        }
    })
    

}
)



app.post('/:id',  function(req, res){
    
    var url = require('url');
    req.requrl = url.parse(req.url, true);
    
    req.player = req.requrl.query.player;
    
    req.score =req.requrl.query.score;
    var id = req.params.id;
    var player = req.player
    var score = req.score
    console.log(score == null)

    if(player == null || score == null)
    {
        res.writeHead(422 , "player or score not introduced", {'Content-Type': 'text/plain'})
        res.end();
    }
    else
    {
        db.query('INSERT INTO "record" ("appcode", "player", "score") VALUES($1, $2, $3)', [id, player, score], (err)=>
    {
        
        if(err)
        {
            res.writeHead(500 , "error in database")
            res.end();
        }
        else
        {
            res.send("query succes!")
        }
    });
    }

    
})

app.get('/:id', function(req, res)
{
    var id = req.params.id;
    db.query('SELECT * FROM app WHERE "app_code" = $1', [id], function(err, resp)
    {
       
       if(resp.rowCount == 0)
       {
           res.writeHead(404, "This app dont exist")
           res.end()
       }    
       else
       {
        db.query('SELECT player, score FROM record WHERE appcode = $1', [id], function(err, resps)
        {
               res.send(resps.rows)
        })
       }
    })
    
    

}
)


app.listen(port);

